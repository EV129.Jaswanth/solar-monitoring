#!/bin/bash

PREV_TOTAL=0
PREV_IDLE=0



CPU=($(sed -n 's/^cpu\s//p' /proc/stat))
IDLE=${CPU[3]}
TOTAL=0
for VALUE in "${CPU[@]:0:8}"; do
TOTAL=$((TOTAL+VALUE))
done

DIFF_IDLE=$((IDLE-PREV_IDLE))
DIFF_TOTAL=$((TOTAL-PREV_TOTAL))
DIFF_USAGE=$(((1000*(DIFF_TOTAL-DIFF_IDLE)/DIFF_TOTAL+5)/10))


PREV_TOTAL="$TOTAL"
PREV_IDLE="$IDLE"

rom=$(df | grep /dev/root | awk '{print $5 }');
t=$(vcgencmd measure_temp | cut -c6-9);
#clear
#cho "CPU:   RAM:      ROM:    Temperature: "
#echo "$DIFF_USAGE    $(free | grep Mem | awk '{print $3/$2 * 100.0}')       ${rom//%}      $t"



echo "$DIFF_USAGE,$(free | grep Mem | awk '{print $3/$2 * 100.0}'),${rom//%},$t"
